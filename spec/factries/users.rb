FactoryBot.defiene do
  factory :user do
    full_name { 'テストユーザー' }
    name { 'テストユーザー' }
    email { 'test@example.com' }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
