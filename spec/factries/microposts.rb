FactoryBot.defiene do
  factory :micropost do
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.png')) }
    body { 'テスト説明文' }
    user
  end
end
