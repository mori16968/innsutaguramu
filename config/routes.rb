Rails.application.routes.draw do


  devise_for :users, controllers: { registrations: 'users/registrations',
                                    sessions: 'users/sessions',
                                    omniauth_callbacks: 'users/omniauth_callbacks' }
  root 'home#top'
  get 'home/policy', to: 'home#policy'
  resources :microposts do
    resources :comments
    resource :likes, only: [:create, :destroy]
  end
  resources :users do
    member do
      get :following, :followers, :prof_detail
    end
  end
  resources :relationships, only: [:create, :destroy]
  resources :notifications, only: :index
end
