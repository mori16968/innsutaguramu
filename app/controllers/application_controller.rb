class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_search

  def set_search
    @search = User.ransack(params[:q])
    @users = @search.result
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:full_name, :name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:full_name, :user_name, :url, :introduce, :sex, :tel])
  end
end
