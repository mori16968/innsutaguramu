class UsersController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @title = "ユーザー一覧"
    @users = User.paginate(page: params[:page], per_page: 5)
    @search = User.ransack(params[:q])
    @users = @search.result
  end

  def show
    @title = "マイページ"
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def prof_detail
    @title = "Profile Details"
    @user = User.find(params[:id])
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
end
