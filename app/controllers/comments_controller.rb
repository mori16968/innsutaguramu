class CommentsController < ApplicationController
  
  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comments_params)
    @comment.user_id = current_user.id
    if @comment.save
      @micropost.create_notification_comment(current_user, @comment.id)
      render :index
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      render :index
    end
  end

  private
    def comments_params
      params.require(:comment).permit(:content, :micropost_id, :user_id)
    end
end
