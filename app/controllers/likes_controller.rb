class LikesController < ApplicationController
  before_action :authenticate_user!

  def create
    like = current_user.likes.build(micropost_id: params[:micropost_id], user_id: current_user.id)
    like.save
    micropost = Micropost.find(params[:micropost_id])
    micropost.create_notification_like(current_user)
    redirect_back(fallback_location: root_url)
  end

  def destroy
    like = Like.find_by(micropost_id: params[:micropost_id])
    like.destroy
    redirect_back(fallback_location: root_url)
  end
end
