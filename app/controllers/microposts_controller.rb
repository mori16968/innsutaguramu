class MicropostsController < ApplicationController
  before_action :authenticate_user!

  def new
    @title = "新規投稿"
    @micropost = Micropost.new
  end

  def create
    @micropost = Micropost.new(micropost_params)
    @micropost.user_id = current_user.id
    @micropost.save
    redirect_to microposts_path
  end

  def index
    @title = "タイムライン"
    if user_signed_in?
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def show
    @title = "投稿詳細"
    @micropost = Micropost.find(params[:id])
    @user = @micropost.user
    @comment = Comment.new
    @comments = @micropost.comments
  end

  def destroy
    @micropost = Micropost.find_by(id: params[:id])
    @micropost.destroy
    redirect_to microposts_path
  end

  private
    def micropost_params
      params.require(:micropost).permit(:body, :picture)
    end
end
