class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :uid, :string
    add_column :users, :provider, :string
    add_column :users, :full_name, :string
    add_column :users, :name, :string
    add_column :users, :url, :string
    add_column :users, :sex, :string
    add_column :users, :introduce, :text
    add_column :users, :tel, :string
  end
end
